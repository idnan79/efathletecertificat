﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFOsloClassDemo.Migrations
{
    public partial class MovieApp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Certifications",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Coaches",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    DOB = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Awards = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coaches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Athletes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    DOB = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Records = table.Column<int>(type: "int", nullable: false),
                    CoachId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Athletes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Athletes_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CoachCertification",
                columns: table => new
                {
                    CoachId = table.Column<int>(type: "int", nullable: false),
                    CertificationId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoachCertification", x => new { x.CoachId, x.CertificationId });
                    table.ForeignKey(
                        name: "FK_CoachCertification_Certifications_CertificationId",
                        column: x => x.CertificationId,
                        principalTable: "Certifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoachCertification_Coaches_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coaches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Certifications",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Boxing" },
                    { 2, "Running" },
                    { 3, "Badass" }
                });

            migrationBuilder.InsertData(
                table: "Coaches",
                columns: new[] { "Id", "Awards", "DOB", "Gender", "Name" },
                values: new object[,]
                {
                    { 1, 10, new DateTime(1981, 6, 2, 0, 20, 49, 425, DateTimeKind.Local).AddTicks(794), "Male", "John McIntyre" },
                    { 2, 15, new DateTime(1991, 6, 2, 0, 20, 49, 441, DateTimeKind.Local).AddTicks(1634), "Female", "Renate Blindheim" },
                    { 3, 52, new DateTime(1947, 6, 2, 0, 20, 49, 441, DateTimeKind.Local).AddTicks(1849), "Male", "Phil Jackson" },
                    { 4, 38, new DateTime(1986, 6, 2, 0, 20, 49, 441, DateTimeKind.Local).AddTicks(1898), "Female", "Christine Girard" }
                });

            migrationBuilder.InsertData(
                table: "Athletes",
                columns: new[] { "Id", "CoachId", "DOB", "Gender", "Name", "Records" },
                values: new object[,]
                {
                    { 3, 2, new DateTime(1971, 9, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Kjetil Andre Aamodt", 28 },
                    { 1, 3, new DateTime(1963, 2, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Micheal Jordan", 10 },
                    { 5, 1, new DateTime(1986, 8, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Usain Bolt", 42 },
                    { 2, 4, new DateTime(1971, 10, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "Thomas Ulsrud", 15 },
                    { 4, 1, new DateTime(1975, 5, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Male", "David Beckham", 52 }
                });

            migrationBuilder.InsertData(
                table: "CoachCertification",
                columns: new[] { "CertificationId", "CoachId" },
                values: new object[,]
                {
                    { 3, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 3, 4 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 3 },
                    { 2, 1 },
                    { 1, 4 },
                    { 2, 4 },
                    { 1, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Athletes_CoachId",
                table: "Athletes",
                column: "CoachId");

            migrationBuilder.CreateIndex(
                name: "IX_CoachCertification_CertificationId",
                table: "CoachCertification",
                column: "CertificationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Athletes");

            migrationBuilder.DropTable(
                name: "CoachCertification");

            migrationBuilder.DropTable(
                name: "Certifications");

            migrationBuilder.DropTable(
                name: "Coaches");
        }
    }
}
