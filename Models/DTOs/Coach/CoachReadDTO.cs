﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFOsloClassDemo.Models.DTOs.Coach
{
    public class CoachReadDTO
    {
        public int Id { get; set; }  //Movies has to be here

        public string Name { get; set; }

        public DateTime DOB { get; set; }

        public string Gender { get; set; }

        public int Awards { get; set; }
        //List to represent franches
        public List<int> Athletes { get; set; }
        //List to represent characters
        public List<int> Certifications { get; set; }


    }
}
