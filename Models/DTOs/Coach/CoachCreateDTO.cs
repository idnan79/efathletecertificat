﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFOsloClassDemo.Models.DTOs.Coach
{
    public class CoachCreateDTO
    {
        public string Name { get; set; }

        public DateTime DOB { get; set; }

        public string Gender { get; set; }

        public int Awards { get; set; }
    }
}
