﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFOsloClassDemo.Models.DTOs.Certification
{
    public class CertificationCreateDTO
    {
        public string Name { get; set; }
    }
}
