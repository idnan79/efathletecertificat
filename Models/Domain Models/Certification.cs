﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EFOsloClassDemo.Models.Domain_Models
{
    public class Certification
    {
        // PK
        public int Id { get; set; }
        // Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        // Relationships
       
        /// <summary>
        /// M2m Coaches to Certifices
        /// </summary>
        public ICollection<Coach> Coaches { get; set; }
    }
}
