﻿using AutoMapper;
using EFOsloClassDemo.Models.Domain_Models;
using EFOsloClassDemo.Models.DTOs.Athlete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFOsloClassDemo.Profiles
{
    public class AthleteProfile : Profile
    {
        public AthleteProfile()
        {
            CreateMap<Athlete, AthleteReadDTO>()
                .ForMember(adto=> adto.Coach,opt=>opt
                .MapFrom(a=> a.CoachId))
                .ReverseMap();
            //Athlete<->AthleteCreateDTO
            CreateMap<Athlete, AthleteCreateDTO>()
                .ForMember(adto => adto.Coach, opt => opt
                  .MapFrom(a => a.CoachId))
                .ReverseMap();
            //Athlete<->AtheleteEditDTO
            CreateMap<Athlete, AthleteEditDTO>()
                .ForMember(adto => adto.Coach, opt => opt
                  .MapFrom(a => a.CoachId))
                .ReverseMap();

            //Adding in controler this mapping
        }
    }
}
