﻿using AutoMapper;
using EFOsloClassDemo.Models.Domain_Models;
using EFOsloClassDemo.Models.DTOs.Coach;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFOsloClassDemo.Profiles
{
    public class CoachProfile : Profile
    {
        public CoachProfile()
        {
           //Relations M2M,, Mapping 
            CreateMap<Coach, CoachReadDTO>()
                .ForMember(cdto => cdto.Athletes, opt=>opt
                .MapFrom(c=>c.Athletes.Select(a=>a.Id).ToList()))
                .ForMember(cdto => cdto.Certifications, opt => opt
                .MapFrom(c => c.Certifications.Select(Cert => Cert.Id).ToList()))
                .ReverseMap();

            CreateMap<Coach, CoachCreateDTO>()
                .ReverseMap();
        }
    }
}
