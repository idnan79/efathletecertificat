﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFOsloClassDemo.Models;
using EFOsloClassDemo.Models.Domain_Models;
using System.Net.Mime;
using AutoMapper;
using EFOsloClassDemo.Models.DTOs.Athlete;

namespace EFOsloClassDemo.Controllers
{
    [Route("api/v1/athletes")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class AthletesController : ControllerBase
    {
        private readonly TrainerDbContext _context;
        private readonly IMapper _mapper;
        //Adding mapper to use in controler
        public AthletesController(TrainerDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

       /// <summary>
       /// Get All Athletes
       /// </summary>
       /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AthleteReadDTO>>> GetAthletes()
        {
            //IEnumerable so inclose in list
            return _mapper.Map<List<AthleteReadDTO>>(await _context.Athletes.ToListAsync());
            //coachId -> " /api/v1/coaches/|1 "
        }

       /// <summary>
       /// Get specific Athlete by Id
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Athlete>> GetAthlete(int id)
        {
            var athlete = await _context.Athletes.FindAsync(id);

            if (athlete == null)
            {
                return NotFound();
            }

            return athlete;
        }

        /// <summary>
        /// Update an Athlete
        /// </summary>
        /// <param name="id"></param>
        /// <param name="athleteDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAthlete(int id, AthleteEditDTO athleteDto)
        {
            if (id != athleteDto.Id)
            {
                return BadRequest();
            }
            //Map to Domain

            Athlete domainAthlete = _mapper.Map<Athlete>(athleteDto);
            _context.Entry(domainAthlete).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AthleteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Add a new Athlete
        /// </summary>
        /// <param name="athlete"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Athlete>> PostAthlete(Athlete athlete)
        {
            _context.Athletes.Add(athlete);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetAthlete), new { id = athlete.Id }, athlete);
        }

       /// <summary>
       /// Delete an Athlete by Id
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAthlete(int id)
        {
            var athlete = await _context.Athletes.FindAsync(id);
            if (athlete == null)
            {
                return NotFound();
            }

            _context.Athletes.Remove(athlete);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool AthleteExists(int id)
        {
            return _context.Athletes.Any(e => e.Id == id);
        }
    }
}
